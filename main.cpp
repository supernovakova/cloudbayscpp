#include <iostream>
#include <sqlite3.h>
#include <stdio.h>


static int callback(void* data, int argc, char** argv, char** azColName) 
{ 
    for (int i = 0; i < argc; i++) { 
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL"); 
    } 
  
    printf("\n"); 
    return 0; 
} 


int main()
{
    // database and table initialization
    sqlite3* db;
    int exit = 0;
    std::string sql = "CREATE TABLE IF NOT EXISTS bays(id TEXT PRIMARY KEY, ticketID TEXT DEFAULT 0, taskID TEXT DEFAULT 0, name TEXT DEFAULT 'n/a', size TEXT NOT NULL, notes TEXT DEFAULT 'n/a')";
    std::string query = "SELECT * FROM bays;";
    
    exit = sqlite3_open("bays.db", &db);

    char* messageError;

    exit = sqlite3_exec(db, sql.c_str(), NULL, 0, &messageError);

    // first query
    sql = "INSERT INTO bays VALUES('1', '1234', '4321', 'testdrive', '500GB', 'no notes');";
    exit = sqlite3_exec(db, sql.c_str(), NULL, 0, &messageError);

    // select query
    exit = sqlite3_exec(db, query.c_str(), callback, NULL, NULL);
    
    

    sqlite3_close(db);
}